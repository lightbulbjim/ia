// =============================================================================
// Copyright 2011-2019 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef PATHS_HPP
#define PATHS_HPP

#include <string>

namespace paths
{

extern const std::string g_gfx_dir;

extern const std::string g_fonts_dir;
extern const std::string g_tiles_dir;
extern const std::string g_images_dir;

extern const std::string g_logo_img_path;
extern const std::string g_skull_img_path;

extern const std::string g_audio_dir;

extern const std::string g_data_dir;

extern const std::string g_user_dir;

extern const std::string g_save_file_path;

extern const std::string g_config_file_path;

extern const std::string g_highscores_file_path;

} // paths

#endif // PATHS_HPP
