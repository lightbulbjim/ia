// =============================================================================
// Copyright 2011-2019 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef STATUS_LINES_HANDLER_HPP
#define STATUS_LINES_HANDLER_HPP

namespace status_lines
{

void draw();

} // status_lines


#endif // STATUS_LINES_HPP
